package com.merritz.servlets;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.Emprunt;
import com.merritz.beans.HachageEmpruntAs;
import com.merritz.beans.ListeEmpruntA;
import com.merritz.beans.Livre;
import com.merritz.beans.Adherant;

public class CreerEmprunt extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		String codeE = request.getParameter("codeE");
		String codeL = request.getParameter("codeB");
		String dateE = request.getParameter("dateO");
		String dateR = request.getParameter("dateB");
		String matricule = request.getParameter("matricule");
		String nom = request.getParameter("nom");
		String message = "Insertion has succeed!";
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		HashMap<String, Emprunt> hashE = (HashMap<String, Emprunt>)session.getAttribute("emprunt");
		@SuppressWarnings("unchecked")
		HashMap<String, Adherant> hashA = (HashMap<String, Adherant>)session.getAttribute("adherant");
		@SuppressWarnings("unchecked")
		HashMap<String, Livre> hashL = (HashMap<String, Livre>)session.getAttribute("livre");
		HachageEmpruntAs hea = (HachageEmpruntAs)session.getAttribute("tableHachEA");
		
		Adherant adh = new Adherant();
		adh.setMatricule(matricule);
		adh.setNom(nom);
		Emprunt emp = new Emprunt();
		emp.setCodeE(codeE);
		emp.setCodeL(codeL);
		emp.setDateE(dateE);
		emp.setDateR(dateR);

		if(hashA.get(matricule) != null){
			//le client qui veut realisez le pret possede cette autorisation
			if(hashL.get(codeL) != null){
				//le livre que l'on veut empreunter existe dans notre base de donn�e (hihihi ici notre variable de session)
				//ajout de l'emprunt dans la liste des prets
				hashE.put(codeE, emp);
				//association de l'emprunt a un adherant
				if(hea.getHachEmpA().get(adh.getMatricule()) != null){
					//si dans la variable de session Hacha... il y'a un champs
					//indexe par le matricule de l'adherant , alors il a deja au
					//moins un pret donc on met juste a jour ca table de pret
					hea.getHachEmpA().get(adh.getMatricule()).getListeE().put(codeE, emp);
				}else{
					//c'est le premier pret de la personne
					HashMap<String, Emprunt> le = new HashMap<String, Emprunt>();
					ListeEmpruntA l = new ListeEmpruntA();
					l.setAd(adh);
					le.put(codeE, emp);
					l.setListeE(le);
					hea.getHachEmpA().put(adh.getMatricule(), l);
				}
			}else{
				message = "D�sol� !<br>Nous n'avons pas repertori� cet article!";
			}
		}else{
			message = "D�sol� !<br>Seul les adherants peuvent effectuer des prets!";
		}
		
		session.setAttribute("adherant", hashA);
		session.setAttribute("livre", hashL);
		session.setAttribute("emprunt", hashE);
		session.setAttribute("tableHachEA", hea);
		request.setAttribute("session", session);
		request.setAttribute("message", message);
		

		request.getServletContext().getRequestDispatcher("/menu.jsp").forward(request, response);
	}
}
