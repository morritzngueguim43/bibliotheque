package com.merritz.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.Adherant;

public class CreerAdherant extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		//recuperation des parametre de la requete
		String matricule = request.getParameter("matricule");
		String nom = request.getParameter("nom");
		String message = "Insertion has succeed!";
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		HashMap<String, Adherant> adh = (HashMap<String, Adherant>)session.getAttribute("adherant");
		
		if(!nom.trim().isEmpty()){
			Adherant a = new Adherant();
			a.setMatricule(matricule);
			a.setNom(nom);
			
			//Ajout de l'adherant dans la liste
			adh.put(matricule, a);
			
		}else{
			message = "Inscription has failed!<br>Possible empty entry!";
		}
		
		//remise de la variable en session
		session.setAttribute("adherant", adh);
		request.setAttribute("session", session);
		request.setAttribute("message", message);
		
		request.getServletContext().getRequestDispatcher("/menu.jsp").forward(request, response);
	}
}
