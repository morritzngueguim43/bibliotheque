package com.merritz.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.Adherant;
import com.merritz.beans.Emprunt;
import com.merritz.beans.HachageEmpruntAs;
import com.merritz.beans.ListeEmpruntA;
import com.merritz.beans.Livre;

public class RechercherEA extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		//recuperation des parametres de la requete
		String param = request.getParameter("option");
		String link = "/afficheAllOwnA.jsp";
		
		//recuperation des parametre de session de l'application
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		HashMap<String, Adherant> adh = (HashMap<String, Adherant>)session.getAttribute("adherant");
		@SuppressWarnings("unchecked")
		HashMap<String, Emprunt> emp = (HashMap<String, Emprunt>)session.getAttribute("emprunt");
		@SuppressWarnings("unchecked")
		HashMap<String, Livre> livre = (HashMap<String, Livre>)session.getAttribute("livre");
		HachageEmpruntAs hea = (HachageEmpruntAs)session.getAttribute("tableHachEA");
		
		//initialisation si variable de session inexistante
		if(adh == null){
			adh = new HashMap<String, Adherant>();
		}
		if(emp == null){
			emp = new HashMap<String, Emprunt>();
		}
		if(livre == null){
			livre = new HashMap<String, Livre>();
		}
		if(hea == null){
			hea = new HachageEmpruntAs();
			HashMap<String, ListeEmpruntA> hle = new HashMap<String, ListeEmpruntA>();
			hea.setHachEmpA(hle);
		}
		
		System.out.println(hea.getHachEmpA());
		System.out.println(adh);
		System.out.println(emp);
		System.out.println(livre);
		//gestion des redirection des items du menu
		if(link != null){
			if(param.equals("recordA")){
				link = "/creationA.jsp";
			}else if(param.equals("recordE")){
				link = "/creationE.jsp";
			}else if(param.equals("recordL")){
				link = "/creationL.jsp";
			}else if(param.equals("infosA") || param.equals("infosEA")){
				link = "/demandeInfoA.jsp";
			}else if(param.equals("infosE")){
				link = "/demandeInfoE.jsp";
			}else if(param.equals("infosL")){
				link = "/demandeInfoL.jsp";
			}else if(param.equals("updateA")){
				link = "/updateA.jsp";
			}else if(param.equals("updateE")){
				link = "/updateE.jsp";
			}else if(param.equals("updateL")){
				link = "/updateL.jsp";
			}
		}
		
		//remise des variables en session et dans l'url
		session.setAttribute("adherant", adh);
		session.setAttribute("emprunt", emp);
		session.setAttribute("livre", livre);
		session.setAttribute("tableHachEA", hea);
		request.setAttribute("session", session);
		
		request.getServletContext().getRequestDispatcher(link).forward(request, response);
	}
}
