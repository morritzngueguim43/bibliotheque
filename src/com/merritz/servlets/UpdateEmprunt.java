package com.merritz.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.Adherant;
import com.merritz.beans.Emprunt;
import com.merritz.beans.HachageEmpruntAs;
import com.merritz.beans.Livre;

public class UpdateEmprunt extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		String matricule = request.getParameter("matricule");
		String codeB = request.getParameter("codeB");
		String codeE = request.getParameter("codeE");
		String dateB = request.getParameter("dateB");
		String message = "Mise a jour effectuer avec succes!";
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		HashMap<String, Adherant> adh = (HashMap<String, Adherant>)session.getAttribute("adherant");
		@SuppressWarnings("unchecked")
		HashMap<String, Emprunt> emp = (HashMap<String, Emprunt>)session.getAttribute("emprunt");
		@SuppressWarnings("unchecked")
		HashMap<String, Livre> livre = (HashMap<String, Livre>)session.getAttribute("livre");
		HachageEmpruntAs hea = (HachageEmpruntAs)session.getAttribute("tableHachEA");
		
		if(adh.get(matricule) != null){
			//l'adherant qui a realisez cet emprunt existe
			if(livre.get(codeB) != null){
				//le livre existe bien
				if(emp.get(codeE) != null){
					//l'emprunt a bien �t� realis�
					Emprunt e = emp.get(codeE);
					e.setDateR(dateB);
					hea.getHachEmpA().get(matricule).getListeE().remove(codeE);
					hea.getHachEmpA().get(matricule).getListeE().put(codeE, e);
				}else{
					message = "Attention!!!<br>Own not found!";
				}
			}else{
				message = "Attention!!!<br>Book not found!";
			}
		}else{
			message = "Attention!!!<br>Adherant not found!";
		}
		
		session.setAttribute("emprunt", emp);
		session.setAttribute("tableHachEA", hea);
		request.setAttribute("session", session);
		request.setAttribute("message", message);
		
		request.getServletContext().getRequestDispatcher("/menu.jsp").forward(request, response);
	}
}
