package com.merritz.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.Livre;

public class RechercherL extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		String codeL = request.getParameter("codeL");
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		HashMap<String, Livre> livre = (HashMap<String, Livre>)session.getAttribute("livre");
		
		if(livre.get(codeL) != null){
			request.setAttribute("livre", livre.get(codeL));
		}else{
			request.setAttribute("message", "No books!!!");
		}
		
		request.getServletContext().getRequestDispatcher("/afficheInfoL.jsp").forward(request, response);
		
	}
}
