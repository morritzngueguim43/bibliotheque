package com.merritz.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.merritz.beans.Livre;

public class CreerLivre extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		//recuperation des parametre de la requete
		String code = request.getParameter("code");
		String intitule = request.getParameter("nom");
		String message = "Insertion has succeed!";
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		HashMap<String, Livre> livre = (HashMap<String, Livre>)session.getAttribute("livre");
		
		if(!intitule.trim().isEmpty()){
			Livre a = new Livre();
			a.setCode(code);
			a.setIntitule(intitule);
			
			//Ajout du livre dans la liste
			livre.put(code, a);
			
		}else{
			message = "Fnscription has failed!<br>Possible empty entry!";
		}
		
		//remise de la variable en session
		session.setAttribute("livre", livre);
		request.setAttribute("session", session);
		request.setAttribute("message", message);
		
		request.getServletContext().getRequestDispatcher("/menu.jsp").forward(request, response);
	}
}
