package com.merritz.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.Livre;

public class UpdateLivre extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		String code = request.getParameter("code");
		String intitule = request.getParameter("nom");
		String message = "Update book successfuly";
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		HashMap<String, Livre> hashL = (HashMap<String, Livre>)session.getAttribute("livre");
		
		Livre livre = new Livre();
		livre.setCode(code);
		livre.setIntitule(intitule);
		if(hashL.get(code) == null){
			message += "<br>This book doesn't exist!<br>We have create it!";
		}
		hashL.put(code, livre);
		
		session.setAttribute("livre", hashL);
		request.setAttribute("session", session);
		request.setAttribute("message", message);
		
		request.getServletContext().getRequestDispatcher("/menu.jsp").forward(request, response);
		
	}
}
