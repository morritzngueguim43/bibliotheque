package com.merritz.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.Emprunt;
import com.merritz.beans.Livre;

public class RechercherE extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		String codeE = request.getParameter("codeE");
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		HashMap<String, Emprunt> emp = (HashMap<String, Emprunt>)session.getAttribute("emprunt");
		@SuppressWarnings("unchecked")
		HashMap<String, Livre> livre = (HashMap<String, Livre>)session.getAttribute("livre");
		
		if(emp.get(codeE) != null){
			request.setAttribute("emprunt", emp.get(codeE));
			String codeL = emp.get(codeE).getCodeL();
			request.setAttribute("livre", livre.get(codeL));
		}else{
			request.setAttribute("message", "Don't know this own code!");
		}
		
		request.getServletContext().getRequestDispatcher("/afficheInfoE.jsp").forward(request, response);
		
	}
}
