package com.merritz.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.Adherant;
import com.merritz.beans.Emprunt;
import com.merritz.beans.HachageEmpruntAs;
import com.merritz.beans.ListeEmpruntA;

public class UpdateAdherant extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		String oldM = request.getParameter("oldMatricule");
		String newM = request.getParameter("newMatricule");
		String nom = request.getParameter("nom");
		String message = "Update book successfuly !";
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		HashMap<String, Adherant> adh = (HashMap<String, Adherant>)session.getAttribute("adherant");
		HachageEmpruntAs hea = (HachageEmpruntAs)session.getAttribute("tableHachEA");
		
		Adherant a = new Adherant();
		a.setMatricule(newM);
		a.setNom(nom);
		
		if(adh.get(oldM) != null){
			adh.remove(oldM);
		}else{
			message = "This Adherant does not exist!<br>We have create it!";
		}
		adh.put(newM, a);
		
		if(hea.getHachEmpA().get(oldM) != null){
			HashMap<String, Emprunt> l = hea.getHachEmpA().get(oldM).getListeE();
			ListeEmpruntA la = new ListeEmpruntA();
			la.setAd(a);
			la.setListeE(l);
			hea.getHachEmpA().remove(oldM);
			hea.getHachEmpA().put(newM, la);
		}else{
			message += "<br>This Adherant has no owns!";
		}
		
		session.setAttribute("adherant", adh);
		session.setAttribute("tableHachEA", hea);
		request.setAttribute("session", session);
		request.setAttribute("message", message);
		
		request.getServletContext().getRequestDispatcher("/menu.jsp").forward(request, response);
		
	}
}
