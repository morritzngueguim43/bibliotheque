package com.merritz.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.merritz.beans.Adherant;
import com.merritz.beans.Emprunt;
import com.merritz.beans.HachageEmpruntAs;
import com.merritz.beans.Livre;

public class RechercherA extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		String matricule = request.getParameter("matricule");
		String option = request.getParameter("option");
		String link = "/menu.jsp";
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		HashMap<String, Adherant> adh = (HashMap<String, Adherant>)session.getAttribute("adherant");
		@SuppressWarnings("unchecked")
		HashMap<String, Livre> livre = (HashMap<String, Livre>)session.getAttribute("livre");
		HachageEmpruntAs hea = (HachageEmpruntAs)session.getAttribute("tableHachEA");
		
		if(option != null){
			if(option.equals("info")){
				link = "/afficheInfoA.jsp";
				if(adh.get(matricule) != null){
					request.setAttribute("adherant", adh.get(matricule));
				}
			}else{
				link = "/afficheAllOwnA.jsp";
				if(adh.get(matricule) != null){
					Iterator<Emprunt> itListeE = hea.getHachEmpA().get(matricule).getListeE().values().iterator();
					String message1 = "";
					while(itListeE.hasNext()){
						Emprunt e = itListeE.next();
						message1 += "<div name=\"containEL\">"
									+ "<div name=\"emprunt\">"
										+ "<label>Own code : "+e.getCodeE()+"</label></br>"
										+ "<label>Book code : "+e.getCodeL()+"</label></br>"
										+ "<label>Own date : "+e.getDateE()+"</label></br>"
										+ "<label>Back date : "+e.getDateR()+"</label></br>"
									+ "</div>"
									+ "<div name=\"livre\">"
										+ "<label>Book code : "+e.getCodeL()+"</label></br>"
										+ "<label>Book's name : "+livre.get(e.getCodeL()).getIntitule()+"</label></br>"
									+ "</div>"
								+ "</div>";
					}
					
					request.setAttribute("listeE", message1);
					request.setAttribute("adherant", adh.get(matricule));
				}else{
					request.setAttribute("message", "No enter disponible!");
				}
			}
		}else{
			request.setAttribute("message", "You must specify the type of the search!");
		}
		
		
		request.getServletContext().getRequestDispatcher(link).forward(request, response);
		
	}
}
