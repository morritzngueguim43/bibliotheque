package com.merritz.beans;

import java.util.HashMap;

public class ListeEmpruntA {
	private Adherant ad;
	private HashMap<String, Emprunt> listeE;
	
	
	/**
	 * @return the ad
	 */
	public Adherant getAd() {
		return ad;
	}
	/**
	 * @param ad the ad to set
	 */
	public void setAd(Adherant ad) {
		this.ad = ad;
	}
	/**
	 * @return the listeE
	 */
	public HashMap<String, Emprunt> getListeE() {
		return listeE;
	}
	/**
	 * @param listeE the listeE to set
	 */
	public void setListeE(HashMap<String, Emprunt> listeE) {
		this.listeE = listeE;
	}
}
