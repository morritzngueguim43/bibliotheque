package com.merritz.beans;

public class Emprunt {
	private String codeE;
	private String codeL;
	private String dateE;
	private String dateR;
	
	
	/**
	 * @return the codeE
	 */
	public String getCodeE() {
		return codeE;
	}
	/**
	 * @param codeE the codeE to set
	 */
	public void setCodeE(String codeE) {
		this.codeE = codeE;
	}
	/**
	 * @return the codeL
	 */
	public String getCodeL() {
		return codeL;
	}
	/**
	 * @param codeL the codeL to set
	 */
	public void setCodeL(String codeL) {
		this.codeL = codeL;
	}
	/**
	 * @return the dateE
	 */
	public String getDateE() {
		return dateE;
	}
	/**
	 * @param dateE the dateE to set
	 */
	public void setDateE(String dateE) {
		this.dateE = dateE;
	}
	/**
	 * @return the dateR
	 */
	public String getDateR() {
		return dateR;
	}
	/**
	 * @param dateR the dateR to set
	 */
	public void setDateR(String dateR) {
		this.dateR = dateR;
	}
}
