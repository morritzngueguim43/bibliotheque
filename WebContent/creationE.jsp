<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="image/ico" href="./images/lib.png">
		<link type="text/css" rel="stylesheet" href="./inc/emprunt.css">
		<title>Record Own</title>
	</head>
	<body>
		<fieldset class="notif">
			<legend><img alt="Oops" src="./images/info.ico"></legend>
			${message }<br><br><br><br>
			<label>Made by Genius @Joel_YK</label>
		</fieldset>
		<form action="creationE" method="GET">
			<fieldset class="form">
				<legend><img alt="Oops" src="./images/info.ico"></legend>
				<fieldset class="owner_inf">
					<legend><img alt="Oops" src="./images/user.ico"></legend>
					<div>
						<label for="matricule">Enter Adh�rant Matricul</label>
						<input type="text" id="matricule" name="matricule" placeholder="fr-litt-4555" required>
					</div>
					<div>
						<label for="nom">Enter Adh�rant Name</label>
						<input type="text" id="nom" name="nom" placeholder="Leonhart Annie">
					</div>
				</fieldset>
				<fieldset class="own_inf">
					<legend><img alt="Oops" src="./images/documents.ico"></legend>
					<div>
						<label for="codeE">Enter Own code</label>
						<input type="text" id="codeE" name="codeE" placeholder="Emp-11" required>
					</div>
					<div>
						<label for="codeB">Enter book code</label>
						<input type="text" id="codeB" name="codeB" placeholder="Mappa-E20" required>
					</div>
					<div>
						<label for="dateO">Enter Own day</label>
						<input type="date" id="dateO" name="dateO" placeholder="10-12-2021" required>
					</div>
					<div>
						<label for="dateB">Enter Back day</label>
						<input type="date" id="dateB" name="dateB" placeholder="10-01-2022" required>
					</div>
				</fieldset>
				<button type="submit">Submit</button>
			</fieldset>
		</form>
	</body>
</html>