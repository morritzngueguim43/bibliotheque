<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="image/ico" href="./images/lib.png">
		<link type="text/css" rel="stylesheet" href="./inc/demande.css">
		<title>Search Book</title>
	</head>
	<body>
		<fieldset class="form">
			<legend><img alt="Oops" src="./images/find.ico"></legend>
			<form action="demandeInfoL" method="GET">
				<div>
					<label for="codeL">Enter book's code</label>
					<input type="text" id="codeL" name="codeL" placeholder="Emp-11">
				</div>
				<button type="submit">Submit</button>
			</form>
		</fieldset>
	</body>
</html>