<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="image/ico" href="./images/lib.png">
		<link type="text/css" rel="stylesheet" href="./inc/menu.css">
		<title>Record Book</title>
	</head>
	<body>
		<fieldset class="notif">
			<legend><img alt="Oops" src="./images/info.ico"></legend>
			${message }<br>
			<label>Made by Genius @Joel_YK</label>
		</fieldset>
		<form action="creationL" method="GET">
			<fieldset class="form">
				<legend><img alt="Oops" src="./images/library.ico"></legend>
				<div>
					<label for="code">Enter book code</label>
					<input type="text" id="code" name="code" placeholder="Mappa-E20" required>
				</div>
				<div>
					<label for="nom">Enter book title</label>
					<input type="text" id="nom" name="nom" placeholder="La Mangouste">
				</div>
				<button type="submit">Submit</button>
			</fieldset>
		</form>
	</body>
</html>