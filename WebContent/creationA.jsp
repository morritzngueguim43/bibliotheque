<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="image/ico" href="./images/lib.png">
		<link type="text/css" rel="stylesheet" href="./inc/menu.css">
		<title>Record Adherant</title>
	</head>
	<body>
		<fieldset class="notif">
			<legend><img alt="Oops" src="./images/info.ico"></legend>
			Complete the form to add an adherant...
			${message }<br><br><br>
			<label>More Samples on www.pandacodeur.com</label>
			<br>
			<label>Made by Genius @Joel_YK</label>
		</fieldset>
		<form action="creationA" method="GET">
			<fieldset class="form">
				<legend><img alt="Oops" src="./images/user.ico"></legend>
				<div>
					<label for="matricule">Enter Adherant Matricul</label>
					<input type="text" id="matricule" name="matricule" placeholder="fr-litt-4555" required>
				</div>
				<div>
					<label for="nom">Enter Adherant Name</label>
					<input type="text" id="nom" name="nom" placeholder="Leonhart Annie">
				</div>
				<button type="submit">Submit</button>
			</fieldset>
		</form>
	</body>
</html>