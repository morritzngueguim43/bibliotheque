<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="image/ico" href="./images/lib.png">
		<link type="text/css" rel="stylesheet" href="./inc/menu.css">
		<title>Update Adherant</title>
	</head>
	<body>
		<fieldset class="notif">
			<legend><img alt="Oops" src="./images/info.ico"></legend>
			${message }<br><br><br><br>
			<label>Made by Genius @Joel_YK</label>
		</fieldset>
		<form action="updateA" method="GET">
			<fieldset class="form">
				<legend><img alt="Oops" src="./images/applications2.ico"></legend>
				<div>
					<label for="oldMatricule">Old Adherant Matricul</label>
					<input type="text" id="oldMatricule" name="oldMatricule" placeholder="fr-litt-4555" required>
				</div>
				<div>
					<label for="newMatricule">New Adherant Matricul</label>
					<input type="text" id="newMatricule" name="newMatricule" placeholder="ca-sci-4005" required>
				</div>
				<div>
					<label for="nom">New Adherant Name</label>
					<input type="text" id="nom" name="nom" placeholder="Leonhart Annie">
				</div>
				<button type="submit">Submit</button>
			</fieldset>
		</form>
	</body>
</html>