<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="image/ico" href="./images/lib.png">
		<link type="text/css" rel="stylesheet" href="./inc/demande.css">
		<title>Search Own</title>
	</head>
	<body>
		<fieldset class="form">
			<legend><img alt="Oops" src="./images/find.ico"></legend>
			<form action="demandeInfoE" method="GET">
				<div>
					<label for="codeE">Enter Own code</label>
					<input type="text" id="codeE" name="codeE" placeholder="La Mangouste">
				</div>
				<button type="submit">Submit</button>
			</form>
		</fieldset>
	</body>
</html>